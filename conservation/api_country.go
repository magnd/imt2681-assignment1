package conservation

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"
)

type Country struct {
	Code       string   `json:"alpha2Code"`
	Name       string   `json:"name"`
	Flag       string   `json:"flag"`
	Species    []string `json:"species"`
	SpeciesKey []int    `json:"speciesKey"`
}

type Speciesid struct {
	Results []struct {
		Species    string `json:"species"`
		SpeciesKey int    `json:"speciesKey"`
	} `json:"results"`
}

func (co *Country) duplicate(s string) bool {
	for _, lc := range co.Species {
		if lc == s {
			return true
		}
	}
	return false
}

func recieveApi(w http.ResponseWriter, url string) io.Reader {
	resp, err := http.Get(url)
	if err != nil {
		http.Error(w, "Could not request server", http.StatusNotFound)
	}
	return resp.Body
}

func (co Country) format(w http.ResponseWriter) {
	json_co, err := json.MarshalIndent(co, "", "    ")
	if err != nil {
		http.Error(w, "Could not encode the object", http.StatusBadRequest)
	}
	fmt.Fprint(w, string(json_co))
}

func HandlerCountry(w http.ResponseWriter, r *http.Request) {
	parts := strings.Split(r.URL.Path, "/") // /conservation/v2/country/..
	country_identifier := parts[4]          // ISO number user writes
	limit := parts[5]                       // limit of occurrences

	url_country := "https://restcountries.eu/rest/v2/alpha/" + country_identifier
	url_species := "https://api.gbif.org/v1/occurrence/search?country=" +
		country_identifier + "&limit=" + limit

	var co Country
	var sp Speciesid
	err := json.NewDecoder(recieveApi(w, url_country)).Decode(&co)
	if err != nil {
		http.Error(w, "Could not decode the response from client", http.StatusBadRequest)
	}
	err = json.NewDecoder(recieveApi(w, url_species)).Decode(&sp)
	if err != nil {
		http.Error(w, "Could not decode the response from client", http.StatusBadRequest)
	}

	for _, s := range sp.Results {
		if co.duplicate(s.Species) != true {
			co.Species = append(co.Species, s.Species)
			co.SpeciesKey = append(co.SpeciesKey, s.SpeciesKey)
		}
	}

	co.format(w)

}
