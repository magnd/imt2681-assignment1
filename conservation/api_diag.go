package conservation

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"time"
)

type Diag struct {
	Gbif          int     `json:"gbif"`
	Restcountries int     `json:"restcountries"`
	Version       string  `json:"version"`
	Uptime        float64 `json:"uptime"`
}

type Time struct {
	StartTime time.Time
	Uptime    float64
}

func (t *Time) getStatus(w http.ResponseWriter, url string) int {

	result, err := http.Get(url)
	if err != nil {
		http.Error(w, "Could not retrieve from server", http.StatusBadRequest)
		t.StartTime = time.Now()
	}
	t.Uptime = time.Since(t.StartTime).Seconds()
	return result.StatusCode
}

func (dg Diag) format(w http.ResponseWriter) {
	json_dg, err := json.MarshalIndent(dg, "", "    ")
	if err != nil {
		http.Error(w, "Could not encode the object", http.StatusBadRequest)
	}
	fmt.Fprint(w, string(json_dg))
}

func HandlerDiag(w http.ResponseWriter, r *http.Request) {

	timer := Time{
		StartTime: time.Now(),
	}

	parts := strings.Split(r.URL.Path, "/")
	version := parts[2]

	url_gbif := "https://api.gbif.org/v1/species/"
	url_restcountries := "https://restcountries.eu/rest/v2/"

	var dg Diag

	dg.Gbif = timer.getStatus(w, url_gbif)
	dg.Restcountries = timer.getStatus(w, url_restcountries)

	dg.Version = version

	dg.Uptime = timer.Uptime

	dg.format(w)

}
