package conservation

import (
	//"io/ioutil"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
)

type Species struct {
	Key            int    `json:"key"`
	Kingdom        string `json:"kingdom"`
	Phylum         string `json:"phylum"`
	Order          string `json:"order"`
	Family         string `json:"family"`
	Genus          string `json:"genus"`
	ScientificName string `json:"scientificName"`
	CanonicalName  string `json:"canonicalName"`
	Year           string `json:"bracketYear"`
}

type Year struct {
	Number string `json:"year"`
}

func (sp Species) format(w http.ResponseWriter) {
	json_sp, err := json.MarshalIndent(sp, "", "    ")
	if err != nil {
		http.Error(w, "Could not encode the object", http.StatusBadRequest)
	}
	fmt.Fprint(w, string(json_sp))
}

func HandlerSpecies(w http.ResponseWriter, r *http.Request) {
	parts := strings.Split(r.URL.Path, "/") // /conservation/v2/country/..
	key := parts[4]                         // {:Key}

	url_species := "https://api.gbif.org/v1/species/" + key + "/"
	url_year := "https://api.gbif.org/v1/species/" + key + "/name/"

	//sp_resp := recieveApi(w, url_species)
	//yy_resp := recieveApi(w, url_year)

	var sp Species
	err := json.NewDecoder(recieveApi(w, url_species)).Decode(&sp)
	if err != nil {
		http.Error(w, "Could not decode the response from client", http.StatusBadRequest)
	}
	err = json.NewDecoder(recieveApi(w, url_year)).Decode(&sp)
	if err != nil {
		http.Error(w, "Could not decode the response from client", http.StatusBadRequest)
	}

	sp.format(w)

}
