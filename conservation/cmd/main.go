package main

import (
	"conservation"
	"fmt"
	"log"
	"net/http"
	"os"
)

func main() {

	port := os.Getenv("PORT")
	if port == "" {
		//port = "8080"
		log.Fatal("$PORT must be set")
	}

	http.HandleFunc("/conservation/v1/country/", conservation.HandlerCountry)
	http.HandleFunc("/conservation/v1/species/", conservation.HandlerSpecies)
	http.HandleFunc("/conservation/v1/diag/", conservation.HandlerDiag)

	fmt.Println("Listening on port " + port)
	log.Fatal(http.ListenAndServe(":0.0.0.0", nil))
}
